import { Injectable } from '@angular/core';
import { Aluno } from '../domain/aluno';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { CrudService } from '../view/abstract/crud-service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlunoService extends CrudService<Aluno, number> {
  aluno: Aluno;
  alunoSelecionado: Aluno;
  constructor(public http: HttpClient, private locate: Location) {
    super(http, environment.urlApi);
  }

  salvarAluno() {
    this.save(this.aluno).subscribe(
      obj => {
        this.aluno = obj;
        this.cancelar();
      },
      error => console.log(error)
    );
    this.aluno = new Aluno();
  }

  getEditarAluno() {
    return this.alunoSelecionado == null ? null : this.alunoSelecionado.id;
  }

  cancelar() {
    this.locate.back();
  }
}
