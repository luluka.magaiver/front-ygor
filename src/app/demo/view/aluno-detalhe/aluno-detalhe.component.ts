import { BreadcrumbService } from './../../../breadcrumb.service';
import { AlunoService } from './../../service/aluno.service';
import { Aluno } from '../../domain/aluno';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public alunoService: AlunoService,
    private breadcrumbService: BreadcrumbService
  ) {
    this.breadcrumbService.setItems([{ label: '' }]);
  }

  ngOnInit() {
    this.alunoService.aluno = new Aluno();
    if (this.router.url !== '/aluno/novo') {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Aluno/editar', routerLink: ['/aluno'] }
      ]);
      const id = this.route.snapshot.paramMap.get('id');
      this.alunoService.findOne(parseInt(id, 0)).subscribe(
        obj => {
          this.alunoService.aluno = obj;
        },
        error => console.log(error)
      );
    } else {
      this.breadcrumbService.setItems([
        { label: 'Cadastro' },
        { label: 'Aluno/novo', routerLink: ['/aluno'] }
      ]);
    }
  }
}
