export class Aluno {
  id: number;
  nome: string;
  telefone: string;
  endereco: string;
  cpf: string;
  email: string;
  senha: string;
  anoInicio: number;
  curso: string;
}
