import { AlunoDetalheComponent } from './demo/view/aluno-detalhe/aluno-detalhe.component';
import { AuxiliarComponent } from './demo/view/auxiliar/auxiliar.component';
import { CertificadoComponent } from './demo/view/certificado/certificado.component';
import { AlunoComponent } from './demo/view/aluno/aluno.component';
import { TurmaComponent } from './demo/view/turma/turma.component';
import { LoginComponent } from './demo/view/login/login.component';
import { DashboardComponent } from './demo/view/dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'turma', component: TurmaComponent },
  { path: 'aluno', component: AlunoComponent },
  { path: 'aluno/novo', component: AlunoDetalheComponent },
  { path: 'aluno/:id', component: AlunoDetalheComponent },
  { path: 'certificado', component: CertificadoComponent },
  { path: 'auxiliar', component: AuxiliarComponent }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
